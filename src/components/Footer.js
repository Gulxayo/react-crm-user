import React from "react";
import "./Footer.css";
import { Link } from "react-router-dom";
import { Row, Col, Container } from "reactstrap";
function Footer() {
  return (
    <div>
      <footer class="footer">
        <div class="container">
          <div class="row">
            <div class="col-md-4">
              <div class="copyRight">
                <p>
                  Website made by Gulxayo Xamzayeva{" "}
                  <script> document.write(new Date().getFullYear());</script>
                </p>
              </div>
            </div>
            <div class="col-md-4">
              <div class="social-networks">
                {/* <a href="#" class="network">
                  <i class="fab fa-youtube"></i>
                </a>
                <a href="#" class="network">
                  <i class="fab fa-instagram"></i>
                </a>
                <a href="#" class="network">
                  <i class="fab fa-github"></i>
                </a> */}
                <Link
                  class="network"
                  to="/"
                  target="_blank"
                  aria-label="Youtube"
                >
                  <i class="fab fa-youtube" />
                </Link>
                <Link
                  class="network"
                  to="/"
                  target="_blank"
                  aria-label="Youtube"
                >
                  <i class="fab fa-instagram"></i>
                </Link>
                <Link
                  class="network"
                  to="/"
                  target="_blank"
                  aria-label="Youtube"
                >
                  <i class="fab fa-github"></i>
                </Link>
                <Link
                  class="network"
                  to="/"
                  target="_blank"
                  aria-label="Youtube"
                >
                  <i class="fab fa-linkedin" />
                </Link>
                <Link
                  class="network"
                  to="/"
                  target="_blank"
                  aria-label="Youtube"
                >
                  <i class="fab fa-twitter" />
                </Link>
              </div>
            </div>
            <div class="col-md-4 pragrif">
              <Container style={{ color: "#ffffff" }}>
                <h3 className="text-footer">Work Field Akademiyasining afzalliklari:</h3>
                <ul className="ml-4">
                  <li className="text-footer-link">Birinchi darslar mutlaqo bepul.</li>
                  <li className="text-footer-link">Tajribali mutahassislardan ta'lim olish.</li>
                  <li className="text-footer-link">
                    Akademiya yaxshi natija ko'rsata olgan kadrlarni o'z
                    jamoasiga qabul qiladi.
                  </li>
                </ul>
              </Container>
            </div>
          </div>
        </div>
      </footer>
    </div>
  );
}

export default Footer;
