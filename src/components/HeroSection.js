import React from 'react';
import '../App.css';
import './HeroSection.css';


function HeroSection() {
  return (
    <div className='hero-container'>
      <video src='/videos/video-1.mp4' autoPlay loop muted />
      <h1>Kelajakni hozirdan qur</h1>
      <p>Biznesingiz qasr bo'lsa, SMM uning darvozasidir!</p>
    </div>
  );
}

export default HeroSection;
