import React from 'react';
import '../../App.css';
import { Jumbotron, Container } from 'reactstrap';
import {
  Card, Button, CardImg, CardTitle, CardText, CardDeck,
  CardSubtitle, CardBody
} from 'reactstrap';
 import './Blog.css'
const Kurslar = (props) => {
  return (
    <>
    <div>
      <Jumbotron fluid>
        <Container >
          <h1 className="display-3">Eng yangi kurslar</h1>
          <p className="lead text-center">Biz bilan kelajak kasblaridan birini o'rgan!.</p>
        </Container>
      </Jumbotron>
    </div>
    <CardDeck>
      <Card>
        <CardImg top width="100%" src="/images/kurs-1.jpg" alt="Card image cap" />
        <CardBody>
          <CardTitle tag="h5">Grafik dizayn</CardTitle>
          <CardSubtitle tag="h6" className="mb-2 text-muted">Kurs davomida siz quyidagi dasturlarda chiroyli va sifatli dizaynlar qilishni o'rganib olasiz: </CardSubtitle>
          <CardText>• Photoshop</CardText>
          <CardText>• Adobe XD</CardText>
          <CardText>• Adobe Illustrator</CardText>
        </CardBody>
      </Card>
      <Card>
        <CardImg top width="100%" src="/images/kurs-2.jpg" alt="Card image cap" />
        <CardBody>
          <CardTitle tag="h5">Grafik dizayn</CardTitle>
          <CardSubtitle tag="h6" className="mb-2 text-muted">Kurs davomida siz quyidagi dasturlarda chiroyli va sifatli dizaynlar qilishni o'rganib olasiz: </CardSubtitle>
          <CardText>• Photoshop</CardText>
          <CardText>• Adobe XD</CardText>
          <CardText>• Adobe Illustrator</CardText>
        </CardBody>
      </Card>
      <Card>
        <CardImg top width="100%" src="/images/kurs-3.jpg" alt="Card image cap" />
        <CardBody>
          <CardTitle tag="h5">Grafik dizayn</CardTitle>
          <CardSubtitle tag="h6" className="mb-2 text-muted">Kurs davomida siz quyidagi dasturlarda chiroyli va sifatli dizaynlar qilishni o'rganib olasiz: </CardSubtitle>
          <CardText>• Photoshop</CardText>
          <CardText>• Adobe XD</CardText>
          <CardText>• Adobe Illustrator</CardText>
        </CardBody>
      </Card>
      <Card>
        <CardImg top width="100%" src="/images/kurs-4.jpg" alt="Card image cap" />
        <CardBody>
          <CardTitle tag="h5">Grafik dizayn</CardTitle>
          <CardSubtitle tag="h6" className="mb-2 text-muted">Kurs davomida siz quyidagi dasturlarda chiroyli va sifatli dizaynlar qilishni o'rganib olasiz: </CardSubtitle>
          <CardText>• Photoshop</CardText>
          <CardText>• Adobe XD</CardText>
          <CardText>• Adobe Illustrator</CardText>
        </CardBody>
      </Card>
    </CardDeck>
  
    </>
  );
};

export default Kurslar;